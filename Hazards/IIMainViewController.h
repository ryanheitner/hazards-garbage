//
//  IIMainViewController.h
//  Garbage
//
//  Created by Ryan Heitner on 11/19/13.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>



@interface IIMainViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate>

@property (nonatomic, retain) AVAudioPlayer *myAudioPlayer;
@property (nonatomic,strong) CLLocationManager *loctionManager;

@property (weak, nonatomic) IBOutlet UITextField *longLatText;

@property (weak, nonatomic) IBOutlet UITextField *streetText;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *mailText;

- (IBAction)takePhoto:(id)sender;
- (IBAction)sendButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;
- (IBAction)facebookButtonTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *myImageView;

+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
