//
//  IIDataViewController.m
//  Garbage
//
//  Created by Ryan Heitner on 11/19/13.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "IIDataViewController.h"
#import "IIConstants.h"

@interface IIDataViewController ()

@end

@implementation IIDataViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.userDefaults =  [NSUserDefaults standardUserDefaults];

}


- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    BOOL welcomeShown = [self.userDefaults boolForKey:kSplashKey];
    if (!welcomeShown) {
        [[NSUserDefaults standardUserDefaults] setBool:YES
                                                forKey:kSplashKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self performSegueWithIdentifier:@"welcome" sender:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
