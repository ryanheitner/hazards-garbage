//
//  IIMainViewController.m
//  Garbage
//
//  Created by Ryan Heitner on 11/19/13.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "IIMainViewController.h"
#import  "IIConstants.h"
#import <AddressBook/AddressBook.h>


@interface IIMainViewController ()

@end

@implementation IIMainViewController

bool save_first_location;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Hide the Status bar
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
    // demo only
    if ([CLLocationManager authorizationStatus] == 	kCLAuthorizationStatusDenied) {
        NSLog(@"denied");
    } else if ([CLLocationManager authorizationStatus] == 	kCLAuthorizationStatusNotDetermined	) {
        NSLog(@"not determined") ;
    } else if ([CLLocationManager authorizationStatus] == 	kCLAuthorizationStatusRestricted) {
        NSLog(@"restricted") ;
    } else if ([CLLocationManager authorizationStatus] == 	kCLAuthorizationStatusAuthorized) {
        NSLog(@"authorized") ;
    }
    self.longLatText.text = kGPSMessage;
    save_first_location = NO;
    [self initLocation];
    [self.nameText setDelegate:self];
    [self.phoneText setDelegate:self];
    [self.mailText setDelegate:self];


}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];

}


// uitext field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void) initLocation {
    self.loctionManager = [[CLLocationManager alloc]init];
    self.loctionManager.delegate = self;
    self.loctionManager.distanceFilter = kCLDistanceFilterNone ;
    self.loctionManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.loctionManager startUpdatingLocation];
}

// callback method for location
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *myLocation = [locations lastObject];
    self.longLatText.text = [NSString stringWithFormat:@"%f %f",myLocation.coordinate.latitude,myLocation.coordinate.longitude		];
    // we either stop updates when we take the picture or when we find a location after taking the picture
    if (save_first_location) {
        [self.loctionManager stopUpdatingLocation];
        [self getStreetName:self.loctionManager.location.coordinate];
    }
}
// +(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize

- (void)getStreetName :(CLLocationCoordinate2D)coordinate {
    NSString* StrCurrentLongitude=[NSString stringWithFormat: @"%f", coordinate.longitude]; // string value
    NSString* StrCurrentLatitude=[NSString stringWithFormat: @"%f", coordinate.latitude];
    
    NSURL *url =  [NSURL URLWithString:
                   [NSString stringWithFormat:@"%@%@,%@&sensor=true&language=he",kMapApiURL,StrCurrentLatitude,StrCurrentLongitude]];
//    __block  NSString  *myString;
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             self.streetText.text  = [self extractStreetName:data];
             NSLog(@"NSURLRequest found street name");
         // do nothing this is a demo only
         } else if ([data length] == 0 && error == nil) {
             NSLog(@"NSURLRequest zero length");
         }  else if (error != nil && error.code == NSURLErrorTimedOut) {
             NSLog(@"NSURLRequest Timeout");
         }  else if (error != nil) {
             NSLog(@"NSURLRequest error");
         }
         
     }];
    
    
    //process google response, which is JSON IIRC
}
- (NSString *) extractStreetName :(NSData *)data {
    //decode it
    NSError *error;
    NSDictionary *responseDict = [NSJSONSerialization
                                JSONObjectWithData:data
                                options:NSJSONReadingMutableContainers
                                error:&error];
    // The "results" object is an array that contains a single dictionary:
    // "results": [{...}]
    // So first lets get the results array
    NSArray *resultsArray = [responseDict objectForKey:KMapApiResults];
    // Then get the results dictionary
    NSDictionary *resultsDict = [resultsArray objectAtIndex:0];
    
    // Once we have the results dictionary, we can get the formatted address
    NSString *address = [resultsDict objectForKey:KMapApiAddress];
    NSLog(@"address:%@",address);
    return address;
}

-(IBAction)takePhoto :(id)sender

{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    
    // image picker needs a delegate,
    imagePickerController.delegate = self;
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion:nil];
    // If we have a location keep it if not continue looking
    if (![self.longLatText.text isEqualToString:kGPSMessage]) {
        [self.loctionManager stopUpdatingLocation];
        [self getStreetName:self.loctionManager.location.coordinate];
    } else {
        save_first_location = YES;
    }
   
}





- (void) playClickSound{
    //start a background sound
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"cameraclick" ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    self.myAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    self.myAudioPlayer.numberOfLoops = 1;
    [self.myAudioPlayer play];
}
- (IBAction)sendButtonTapped:(id)sender {
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (IBAction)facebookButtonTapped:(id)sender {
}



-(IBAction)chooseFromLibrary:(id)sender
{
    
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc]init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    // image picker needs a delegate so we can respond to its messages
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
}

//delegate methode will be called after picking photo either from camera or library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.myImageView.image = [IIMainViewController imageWithImage:image scaledToSize:self.myImageView.frame.size];
}

// scales the image to size
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
