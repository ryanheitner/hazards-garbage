//
//  main.m
//  Hazards
//
//  Created by Ryan Heitner on 24/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IIAppDelegate class]));
    }
}
