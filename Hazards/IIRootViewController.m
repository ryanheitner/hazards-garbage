//
//  IIRootViewController.m
//  Garbage
//
//  Created by Ryan Heitner on 11/19/13.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "IIRootViewController.h"
#import "IIConstants.h"

@interface IIRootViewController ()

@end


@implementation IIRootViewController



- (void)viewDidLoad
{
    BOOL welcomeShown = [self.userDefaults boolForKey:kSplashKey];
    if (!welcomeShown) {
        [[NSUserDefaults standardUserDefaults] setBool:YES
                                                forKey:kSplashKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self performSegueWithIdentifier:@"welcome" sender:self];
    }
    	
    [super viewDidLoad];
    self.userDefaults =  [NSUserDefaults standardUserDefaults];

    // Hide the Status bar
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    else
    {
        // iOS 6
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}
- (BOOL) prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

@end
